﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab23_2_App25
{
    internal class Matrix
    {
        public static double[,] GetRandomMatrix(int rowsNumber, int columnsNumber, int min = 1, int max = 100)
        {
            double[,] matrix = new double[rowsNumber, columnsNumber];
            Random randint = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < rowsNumber; i++)
            {
                for (int j = 0; j < columnsNumber; j++)
                {
                    matrix[i, j] = randint.Next(min, max);
                }
            }
            return matrix;
        }

        public static void WriteMatrixToGrid(double[,] matrix, DataGridView gridView)
        {
            gridView.RowCount = matrix.GetLength(0);
            gridView.ColumnCount = matrix.GetLength(1);
            for (int i = 0; i < gridView.RowCount; i++)
            {
                for (int j = 0; j < gridView.ColumnCount; j++)
                {
                    gridView[j, i].Value = matrix[i, j];
                }
            }
        }

        public static double[,] ReadMatrixToGrid(DataGridView gridView)
        {
            double[,] matrix = new double[gridView.RowCount, gridView.ColumnCount];
            for (int i = 0; i < gridView.RowCount; i++)
            {
                for (int j = 0; j < gridView.ColumnCount; j++)
                {
                    matrix[i, j] = Convert.ToDouble(gridView[j, i].Value);
                }
            }
            return matrix;
        }

        public static void FillGridWithRandomNumbers(DataGridView gridView, int min = 1, int max = 100)
        {
            double[,] matr = Matrix.GetRandomMatrix(gridView.RowCount, gridView.ColumnCount);
            Matrix.WriteMatrixToGrid(matr, gridView);
        }

        public static void SaveMatrixToFile(string FileName, double[,] matrix)
        {
            StreamWriter sw = new StreamWriter(FileName);
            sw.WriteLine(matrix.GetLength(0));
            sw.WriteLine(matrix.GetLength(1));
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sw.WriteLine(matrix[i, j]);
                }
            }
            sw.Close();
        }

        public static double[,] ReadMatrixFromFile(string FileName)
        {
            StreamReader sr = new StreamReader(FileName);
            int rowsCount = Convert.ToInt32(sr.ReadLine());
            int columnsCount = Convert.ToInt32(sr.ReadLine());
            double[,] matrix = new double[rowsCount, columnsCount];
            for (int i = 0; i < rowsCount; i++)
            {
                for (int j = 0; j < columnsCount; j++)
                {
                    matrix[i, j] = Convert.ToDouble(sr.ReadLine());
                }
            }
            sr.Close();
            return matrix;
        }

        public static void SaveGridToFile(string FileName, DataGridView gridView)
        {
            double[,] matrix = ReadMatrixToGrid(gridView);
            SaveMatrixToFile(FileName, matrix);
        }
        public static void ReadGridDataFromFile(string FileName, DataGridView gridView)
        {
            double[,] matrix = ReadMatrixFromFile(FileName);
            WriteMatrixToGrid(matrix, gridView);
        }
    }
}
